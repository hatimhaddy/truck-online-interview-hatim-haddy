import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";

@Injectable({
  providedIn: "root",
})
export class RestService {
  baseUrl: string = "http://ec2-34-252-249-185.eu-west-1.compute.amazonaws.com";
  httpOptions = {
    headers: new HttpHeaders({
      Accept: "application/json",
      "Content-Type": "application/json",
    }),
  };
  constructor(private httpClient: HttpClient) {}

  public getUserProfile(): Observable<any> {
    return this.httpClient
      .get(this.baseUrl + "/apis/interview/1/users/profile", this.httpOptions)
      .pipe(
        map((response) => {
          return response;
        })
      );
  }

  public getDriverStatus(): Observable<any> {
    return this.httpClient.get(
      this.baseUrl + "/apis/interview/1/status",
      this.httpOptions
    );
  }

  public getDriverActivities() {
    return this.httpClient.get(
      this.baseUrl + "/apis/interview/1/activities",
      this.httpOptions
    );
  }
}
