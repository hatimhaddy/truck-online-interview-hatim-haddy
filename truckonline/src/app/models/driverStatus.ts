import { DriverActivity } from './driverActivity';

export interface DriverStatus {
    vehicleVRN: string;
    trailerVRN: string;
    activity: DriverActivity;
  }
  