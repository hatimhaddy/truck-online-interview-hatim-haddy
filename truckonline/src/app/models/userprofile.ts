export interface UserProfile {
  uid: string;
  firstName: string;
  lastName: string;
  phone: string;
  login: string;
  email: string;
  enabled: boolean;
  createdAt: string;
  templateUid: string;
  locale: string;
  type: string;
  shortName: string;
  licenceNumber: string;
  driverCardId: string;
}
