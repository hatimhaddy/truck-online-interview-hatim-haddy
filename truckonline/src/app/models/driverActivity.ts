export class DriverActivity {
  vehicleVIN: string;
  startDate: string;
  endDate: string;
  drivingSeat: string;
  activity: string;
  source: string;
  label: boolean;
  startKm: number;
  endKm: number;
  tag: string;

  constructor(values: Object = {}) {
    Object.assign(this, values);
  }
}
