import { Component, OnDestroy, OnInit } from "@angular/core";
import { DriverActivity } from "../models/driverActivity";
import { DriverStatus } from "../models/driverStatus";
import { UserProfile } from "../models/userprofile";
import { RestService } from "../rest.service";

@Component({
  selector: "app-home",
  templateUrl: "home.page.html",
  styleUrls: ["home.page.scss"],
})
export class HomePage implements OnInit, OnDestroy {
  userProfile: UserProfile;
  driverStatus: DriverStatus;
  listDriverActivities: DriverActivity[];

  constructor(private restService: RestService) {}

  ionViewWillEnter() {
    this.restService.getUserProfile().subscribe(
      (data: UserProfile) => {
        this.userProfile = data;
        console.log(
          "====== DRIVER SHORT NAME =====> " + this.userProfile.shortName
        );
      },
      (err) => {
        console.error(err);
      }
    );

    this.restService.getDriverStatus().subscribe(
      (data: DriverStatus) => {
        this.driverStatus = data;
        console.log(
          "====== DRIVER VEHICLE VRN =====> " + this.driverStatus.vehicleVRN
        );
      },
      (err) => {
        console.error(err);
      }
    );

    this.restService.getDriverActivities().subscribe(
      (data: DriverActivity[]) => {
        this.listDriverActivities = data;
        console.log("====== LIST OF DRIVER ACTIVITIES =====> ");
        this.listDriverActivities.forEach((driverActivity) => {
          console.log(
            "====== ACTIVITY =====> " +
              driverActivity.activity +
              "  ======  STARTS AT =====> " +
              driverActivity.startDate
          );
        });
      },
      (err) => {
        console.error(err);
      }
    );
  }

  ngOnDestroy() {}

  ngOnInit() {}
}
