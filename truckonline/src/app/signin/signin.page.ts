import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";

@Component({
  selector: "app-signin",
  templateUrl: "./signin.page.html",
  styleUrls: ["./signin.page.scss"],
})
export class SigninPage implements OnInit {
  loginForm: FormGroup;
  constructor(public router: Router) {
    this.buildForm();
  }

  ngOnInit() {}

  login() {
    this.router.navigate(["home"]);
  }

  private buildForm() {
    this.loginForm = new FormGroup({
      login: new FormControl(null, {
        updateOn: "blur",
        validators: [
          Validators.required,
          Validators.pattern("/^w+([.-]?w+)*@w+([.-]?w+)*(.w{2,})+$/"),
        ],
      }),
      password: new FormControl(null, {
        updateOn: "blur",
        validators: [Validators.required],
      }),
      rememberMe: new FormControl(null),
    });
  }
}
