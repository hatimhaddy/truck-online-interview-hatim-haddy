import { Component, OnDestroy, OnInit } from "@angular/core";
import { NavigationEnd, Router } from "@angular/router";
import { Subject } from "rxjs";
import { takeUntil, filter } from "rxjs/operators";

@Component({
  selector: "app-tabs",
  templateUrl: "./tabs.component.html",
  styleUrls: ["./tabs.component.scss"],
})
export class TabsComponent implements OnInit, OnDestroy {
  closed$ = new Subject<any>();
  showTabs = true;

  constructor(private router: Router) {}

  ngOnInit() {
    this.router.events
      .pipe(
        filter((e) => e instanceof NavigationEnd),
        takeUntil(this.closed$)
      )
      .subscribe((event: NavigationEnd) => {
        const currentRoute = this.router.url.toString();
        if (currentRoute === "/" || currentRoute === "/signin") {
          this.showTabs = false;
        } else {
          this.showTabs = true;
        }
      });
  }

  ngOnDestroy() {
    this.closed$.next();
  }
}
