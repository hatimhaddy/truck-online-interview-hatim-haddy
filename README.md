# Truck Online - interview - hatim haddy

Mobile app developed with 'Ionic 5', 'Angular 10' and 'Cordova 6.5.0'.

# Steps to launch the project on VS Code

1- Clone the repo:

- git clone git@gitlab.com:hatimhaddy/truck-online-interview-hatim-haddy.git

or over https:

- git clone https://gitlab.com/hatimhaddy/truck-online-interview-hatim-haddy.git

2- Navigate to the project directory:

- cd truck-online-interview-hatim-haddy/truckonline

3- Install Ionic 5:

- npm install -g ionic@5

4- Install Cordova 6.5.0 (to be able to build Android 7):

- npm install -g cordova@6.5.0

3- Install node modules:

- npm install

4- Add the platform for Android (we used Android 7 "api 26" to bypass http ssl check):

- ionic cordova platform add android@7.0.0

5- To build the apk:

- ionic cordova build android

6- To run the app on emulator (or plugged real device), just make sure the emulator is up and running:

- ionic cordova emulate android

